// requires...
const fs = require('fs');
const path = require('path');

// constants...
const PATH = './files';
const FILE_EXTENSIONS = new Set(['.log', '.txt', '.json', '.yaml', '.xml', '.js']);

function createFile (req, res, next) {
  // Your code to create the file.
  try{
    const name = req.body.filename;
    const data = req.body.content;
    const extension = path.extname(name);

    if(!data) throw Error('Please specify \'content\' parameter');
    if(!FILE_EXTENSIONS.has(extension)) throw Error('Error. Extension is not supported');

    const filePath = path.join(PATH, name);

    fs.writeFileSync(filePath, data);
    res.status(200).send({ 'message': 'File created successfully' })
  }catch (e){
    res.status(400).send({'message': e.message});
  }
};

function getFiles (req, res, next) {
  // Your code to get all files.
  try {
    let files = fs.readdirSync(PATH);

    res.status(200).send({
      "message": "Success",
      "files": files
    });
  }catch (e){
    res.status(400).send({'message': 'Client error'});
  }
};

const getFile = (req, res, next) => {
  // Your code to get all files.
  const name = req.params.filename;
  try {
    const extension = path.extname(name).substring(1);
    const filePath = path.join(PATH,name);
    const data = fs.readFileSync(filePath, 'utf-8');
    const uploadedDate = fs.statSync(filePath).birthtime;

    res.status(200).send({
      "message": "Success",
      "filename": name,
      "content":  data,
      "extension": extension,
      "uploadedDate": uploadedDate
    });
  }catch (e){
    res.status(400).send({'message': `No file with '${name}' found`});
  }
  };

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
